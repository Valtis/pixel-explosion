#!/usr/bin/env bash

if [ $EUID -ne 0 ]; then
	echo "This script installs dependencies and must be run as root"
	exit 1
fi

apt update
apt install gcc g++ libsdl2-dev make
