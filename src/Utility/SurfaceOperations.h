#pragma once
#include <SDL.h>

Uint8 get_alpha(Sint32 x, Sint32 y, SDL_Surface *surface);
Uint8 get_red(Sint32 x, Sint32 y, SDL_Surface *surface);
Uint8 get_green(Sint32 x, Sint32 y, SDL_Surface *surface);
Uint8 get_blue(Sint32 x, Sint32 y, SDL_Surface *surface);

void set_alpha(Sint32 x, Sint32 y, Uint32 color, SDL_Surface *surface);
void set_red(Sint32 x, Sint32 y, Uint32 color, SDL_Surface *surface);
void set_green(Sint32 x, Sint32 y, Uint32 color, SDL_Surface *surface);
void set_blue(Sint32 x, Sint32 y, Uint32 color, SDL_Surface *surface);