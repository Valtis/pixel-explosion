#include "Utility/SurfaceOperations.h"

static void lock_surface(SDL_Surface *surface) {
    if (SDL_MUSTLOCK(surface)) {
        if (SDL_LockSurface(surface) != 0) {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to lock surface: %s", SDL_GetError());
            exit(1);
        }
    }
}

static void unlock_surface(SDL_Surface *surface) {
    if (SDL_MUSTLOCK(surface)) {
        SDL_UnlockSurface(surface);
    }
}

static Uint8 get_color(int x, Sint32 y, Sint32 width, Uint32 *pixels, Uint32 mask, Uint8 shift, Uint8 loss) {
    Uint32 temp;
    temp = pixels[y*width + x] & mask;
    temp = temp >> shift;
    temp = temp << loss;
    return (Uint8)temp;
}


static void set_color(int x, Sint32 y, Sint32 width, Uint32 color, Uint32 *pixels, Uint32 mask, Uint8 shift, Uint8 loss) {
    pixels[y*width + x] = pixels[y*width + x] ^ (pixels[y*width + x] & mask);
    color = color >> loss;
    color = color << shift;
    pixels[y*width + x] = pixels[y*width + x] | (color);
}

Uint8 get_alpha(int x, Sint32 y, SDL_Surface *surface) {
    lock_surface(surface);
    SDL_PixelFormat *fmt;
    fmt = surface->format;
    Uint32 *pixels = (Uint32 * )surface->pixels;

    Uint8 retVal = get_color(x, y, surface->w, pixels, fmt->Amask, fmt->Ashift, fmt->Aloss);

    unlock_surface(surface);

    return retVal;
}

Uint8 get_red(int x, Sint32 y, SDL_Surface *surface) {
    lock_surface(surface);

    SDL_PixelFormat *fmt;
    fmt = surface->format;
    Uint32 *pixels = (Uint32 * )surface->pixels;

    Uint8 retVal = get_color(x, y, surface->w, pixels, fmt->Rmask, fmt->Rshift, fmt->Rloss);

    unlock_surface(surface);
    return retVal;
}


Uint8 get_green(int x, Sint32 y, SDL_Surface *surface) {
    lock_surface(surface);

    SDL_PixelFormat *fmt;
    fmt = surface->format;
    Uint32 *pixels = (Uint32 * )surface->pixels;

    Uint8 retVal = get_color(x, y, surface->w, pixels, fmt->Gmask, fmt->Gshift, fmt->Gloss);

    unlock_surface(surface);
    return retVal;
}



Uint8 get_blue(int x, Sint32 y, SDL_Surface *surface) {
    lock_surface(surface);

    SDL_PixelFormat *fmt;
    fmt = surface->format;
    Uint32 *pixels = (Uint32 * )surface->pixels;

    Uint8 retVal = get_color(x, y, surface->w, pixels, fmt->Bmask, fmt->Bshift, fmt->Bloss);

    unlock_surface(surface);
    return retVal;
}



void set_alpha(int x, Sint32 y, Uint32 color, SDL_Surface *surface) {
    lock_surface(surface);

    SDL_PixelFormat *fmt;
    fmt = surface->format;
    Uint32 *pixels = (Uint32 * )surface->pixels;

    set_color(x, y, surface->w, color, pixels, fmt->Amask, fmt->Ashift, fmt->Aloss);
    unlock_surface(surface);
}


void set_red(int x, Sint32 y, Uint32 color, SDL_Surface *surface) {
    lock_surface(surface);

    SDL_PixelFormat *fmt;
    fmt = surface->format;
    Uint32 *pixels = (Uint32 * )surface->pixels;

    set_color(x, y, surface->w, color, pixels, fmt->Rmask, fmt->Rshift, fmt->Rloss);
    unlock_surface(surface);
}

void set_green(int x, Sint32 y, Uint32 color, SDL_Surface *surface) {
    lock_surface(surface);

    SDL_PixelFormat *fmt;
    fmt = surface->format;
    Uint32 *pixels = (Uint32 * )surface->pixels;

    set_color(x, y, surface->w, color, pixels, fmt->Gmask, fmt->Gshift, fmt->Gloss);
    unlock_surface(surface);
}

void set_blue(int x, Sint32 y, Uint32 color, SDL_Surface *surface) {
    lock_surface(surface);

    SDL_PixelFormat *fmt;
    fmt = surface->format;
    Uint32 *pixels = (Uint32 * )surface->pixels;

    set_color(x, y, surface->w, color, pixels, fmt->Bmask, fmt->Bshift, fmt->Bloss);
    unlock_surface(surface);
}


