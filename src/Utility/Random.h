#pragma once
#include <random>

#define Random RandomImpl::instance()

class RandomImpl final {
public:

    RandomImpl();
    ~RandomImpl() = default;

    RandomImpl(const RandomImpl &) = delete;
    RandomImpl &operator=(const RandomImpl &) = delete;

    RandomImpl(RandomImpl &&) = delete;
    RandomImpl &operator=(RandomImpl &&) = delete;

    static RandomImpl &instance() {
        static RandomImpl instance;
        return instance;
    }


    template <typename T>
    T get_random(T upper_limit, T lower_limit= 0) {
        std::uniform_real_distribution<double> distribution(lower_limit, upper_limit);
        return distribution(m_engine);
    }

private:
    std::mt19937 m_engine;
};
