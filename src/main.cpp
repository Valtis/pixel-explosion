#include <stdio.h>
#include <vector>
#include <chrono>
#include <algorithm>

typedef std::chrono::high_resolution_clock Clock;

#include "Renderer/Renderer.h"
#include "Emitter/Emitter.h"
#include "Emitter/ParticleCache.h"
#include "Utility/Random.h"

//#define REPORT_FPS

#define PARTICLES 1500
#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080

#define CACHED_PARTICLE_TEXTURES 100

#define EMITTER_X SCREEN_WIDTH/2
#define EMITTER_Y SCREEN_HEIGHT/2

#define PARTICLE_LIFETIME_MS 2500
#define MAX_SPEED 0.5f

#define UNUSED(x) (void)(x)

#define FPS_GOAL 120
#define MICROSECONDS_PER_FRAME 1000'000.0f/FPS_GOAL

std::vector<Emitter> initialize();
void run(std::vector<Emitter>);
void update_emitters(std::vector<Emitter> &, float);


int main(int argc, char **argv) {
    UNUSED(argc);
    UNUSED(argv);

    if (SDL_Init(SDL_INIT_VIDEO)) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to initialize SDL: %s", SDL_GetError());
        return 1;
    }

    auto emitters = initialize();
    run(emitters);
    SDL_Quit();
}

std::vector<Emitter> initialize() {
    Renderer.create_window("Emitters", SCREEN_WIDTH, SCREEN_HEIGHT);
    for (int i = 0; i < CACHED_PARTICLE_TEXTURES; ++i) {
        ParticleCache.add_particle(Random.get_random(256), Random.get_random(90), Random.get_random(90));
    }

    std::vector<Emitter> emitters;
    emitters.push_back(std::move(Emitter(PARTICLES,EMITTER_X, EMITTER_Y, PARTICLE_LIFETIME_MS*1000, MAX_SPEED)));

    return emitters;
}

void run(std::vector<Emitter> emitters) {


    float lifetime = 0;
    auto now = Clock::now();

    lifetime = 0;

    #ifdef REPORT_FPS
    Sint32 report_interval = 60;
    Sint32 cur = 0;

    Uint32 fps_ticks = 0;
    #endif

    while (lifetime < PARTICLE_LIFETIME_MS*1000) {
        auto new_time = Clock::now();
        float diff = ((float)std::chrono::duration_cast<std::chrono::microseconds>(new_time - now).count());

        if (diff > MICROSECONDS_PER_FRAME) {
            lifetime += diff;

            update_emitters(emitters, diff);
            Renderer.draw(emitters);
            now = Clock::now();
        }
    }
}


void update_emitters(std::vector<Emitter> &emitters, float diff) {

    emitters.erase(
        std::remove_if(
        emitters.begin(), emitters.end(), [](const Emitter &emitter) { return !emitter.is_alive(); }
    ), emitters.end());


    for (auto &emitter : emitters) {
        emitter.update(diff);
    }
}