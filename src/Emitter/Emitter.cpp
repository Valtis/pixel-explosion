#include "Emitter/Emitter.h"

#include "Utility/Random.h"

#include <SDL.h>
#include <algorithm>
#include <fstream>


Emitter::Emitter(Uint32 particles, Sint32 x, Sint32 y, Uint32 lifetime, float max_speed) : m_x(x), m_y(y) {
    create_particles(particles, lifetime, max_speed);
}

void Emitter::create_particles(Uint32 particles, Uint32 lifetime, float max_speed ) {
    for (Uint32 i = 0; i < particles; ++i) {
        create_particle(lifetime, max_speed);
    }
}

void Emitter::create_particle(Uint32 lifetime, float max_speed ) {
    float velocity = Random.get_random(max_speed, 0.01f);
    float angle  = Random.get_random(2.0f*3.1415926535f);

    auto particle = Particle{(float)m_x, (float)m_y, velocity*cos(angle)/1000.0f, velocity*sin(angle)/1000.0f, lifetime};

    particle.initialize();
    m_particles.push_back(particle);
}

void Emitter::update(Uint32 delta_t) {
    update_particles(delta_t);
    remove_dead_particles();
}

void Emitter::update_particles(Uint32 delta_t) {
    for (auto &particle : m_particles)
    {
        particle.update(delta_t);
    }
}

void Emitter::remove_dead_particles() {
    m_particles.erase(
        std::remove_if(
        m_particles.begin(), m_particles.end(), [](const Particle &particle) { return particle.is_dead(); }
    ), m_particles.end());
}
