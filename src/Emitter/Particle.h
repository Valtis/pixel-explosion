#pragma once
#include <SDL.h>



class Particle final {
public:
    Particle(float x, float y, float x_velocity, float y_velocity, Uint32 life_time);
    ~Particle() = default;

    Particle(const Particle &) = default;
    Particle &operator=(const Particle &) = default;

    Particle(Particle &&) = default;
    Particle &operator=(Particle &&) = default;

    void initialize();

    SDL_Texture *get_texture() const { return m_texture; }
    void update(Uint32 delta_t);
    bool is_dead() const { return m_life_remaining <= 0; }

    Sint32 get_x() const { return (Sint32)m_x; }
    Sint32 get_y() const { return (Sint32)m_y; }

private:
    float m_x;
    float m_y;
    float m_x_velocity;
    float m_y_velocity;
    Uint32 m_life_remaining;

    SDL_Texture *m_texture; // owned by ParticleCache, do not free
};