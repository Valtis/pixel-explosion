#pragma once
#include <vector>
#include <SDL.h>
#include "Emitter/Particle.h"

class Emitter final {
public:
    Emitter(Uint32 particles, Sint32 x, Sint32 y, Uint32 lifetime, float max_speed);
    ~Emitter() = default;

    Emitter(const Emitter &) = default;
    Emitter& operator=(const Emitter &) = default;

    Emitter(Emitter &&) = default;
    Emitter &operator=(Emitter &&) = default;

    void update(uint32_t delta_t);

    bool is_alive() const { return !m_particles.empty(); }

    const std::vector<Particle> &get_particles() const { return m_particles;};


    Sint32 get_x() const { return m_x; }
    Sint32 get_y() const { return m_y; }

private:

    void create_particles(Uint32 particles, Uint32 lifetime, float max_speed);
    void create_particle(Uint32 lifetime, float max_speed);

    void set_color(Particle *particle);

    void remove_dead_particles();
    void update_particles(uint32_t delta_t);

    std::vector<Particle> m_particles;

    Sint32 m_x;
    Sint32 m_y;
};