#include <stdlib.h>

#include "Emitter/ParticleCache.h"
#include "Utility/Random.h"
#include "Utility/SurfaceOperations.h"
#include "Renderer/Renderer.h"



ParticleCacheImpl &ParticleCacheImpl::instance() {
    static ParticleCacheImpl instance;
    return instance;
}

ParticleCacheImpl::~ParticleCacheImpl() {
    for (auto texture : m_texture_cache) {
        SDL_DestroyTexture(texture);
    }
}


void ParticleCacheImpl::add_particle(Sint32 r, Sint32 g, Sint32 b) {
    SDL_Surface *surface = create_surface(r, g, b);

    SDL_Texture *texture = SDL_CreateTextureFromSurface(Renderer.get_rendering_context(), surface);
    SDL_FreeSurface(surface);

    if (texture == nullptr) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to allocate texture for particle emitter: %s", SDL_GetError());
        exit(1);
    }

    m_texture_cache.push_back(texture);
}

SDL_Surface * ParticleCacheImpl::create_surface(Sint32 r, Sint32 g, Sint32 b ) {
    Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

    SDL_Surface *surface = SDL_CreateRGBSurface(0, 1, 1, 32, rmask, gmask, bmask, amask);
    if (surface == nullptr) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to allocate surface for particle emitter: %s", SDL_GetError());
        exit(1);
    }

    set_surface_color(surface, r, g, b);
    return surface;
}


void ParticleCacheImpl::set_surface_color(SDL_Surface * surface, Sint32 r, Sint32 g, Sint32 b) {
    set_red(0, 0, r, surface);
    set_green(0, 0, g, surface);
    set_blue(0, 0, b, surface);
    set_alpha(0, 0, 255, surface);
}


SDL_Texture *ParticleCacheImpl::get_random_texture() const {
    Uint32 pos = Random.get_random(m_texture_cache.size());
    return m_texture_cache[pos];
}