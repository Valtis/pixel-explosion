#include "Emitter/Particle.h"
#include "Emitter/ParticleCache.h"

Particle::Particle(float x, float y, float x_velocity, float y_velocity, Uint32 lifetime) : m_x(x), m_y(y),
    m_x_velocity(x_velocity), m_y_velocity(y_velocity), m_life_remaining(lifetime) {

}


void Particle::initialize() {
    m_texture = ParticleCache.get_random_texture();
}

// delta_t in microseconds
void Particle::update(Uint32 delta_t) {

    float delta_x = m_x_velocity*delta_t;
    float delta_y = m_y_velocity*delta_t;

    m_life_remaining -= delta_t;

    m_x += delta_x;
    m_y += delta_y;
}