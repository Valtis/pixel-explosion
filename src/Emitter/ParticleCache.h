#pragma once
#include <vector>
#include <SDL.h>

#define ParticleCache ParticleCacheImpl::instance()

class ParticleCacheImpl final {
public:
    ~ParticleCacheImpl();

    ParticleCacheImpl(const ParticleCacheImpl &) = delete;
    ParticleCacheImpl &operator=(const ParticleCacheImpl &) = delete;

    ParticleCacheImpl(ParticleCacheImpl &&) = delete;
    ParticleCacheImpl &operator=(ParticleCacheImpl &&) = delete;


    static ParticleCacheImpl &instance();

    void add_particle(Sint32 r, Sint32 g, Sint32 b);
    SDL_Texture *get_random_texture() const;

private:
    ParticleCacheImpl() = default;
    SDL_Surface *create_surface(Sint32 r, Sint32 g, Sint32 b);
    void set_surface_color(SDL_Surface *surface, Sint32 r, Sint32 g, Sint32 b);

    std::vector<SDL_Texture *> m_texture_cache;
};