#include <algorithm>
#include <stdlib.h>

#include "Renderer/Renderer.h"
#include "Emitter/Emitter.h"
#include "Emitter/Particle.h"


RendererImpl::RendererImpl() : m_window(nullptr), m_renderer(nullptr) {
}

RendererImpl::~RendererImpl() {
    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyWindow(m_window);
}


RendererImpl &RendererImpl::instance() {
    static RendererImpl instance;
    return instance;
}


void RendererImpl::create_window(const char *title, Sint32 width, Sint32 height) {

    if (SDL_CreateWindowAndRenderer(
        width,
        height,
        SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_BORDERLESS,
        &m_window,
        &m_renderer) != 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SDL_CreateWindowAndRenderer failed with error: %s", SDL_GetError());
        exit(1);
    }

    if (m_window == nullptr || m_renderer == nullptr) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to create window or render context");
        exit(1);
    }

    SDL_SetWindowTitle(m_window, title);
    SDL_SetWindowPosition(m_window, 0, 0);
}

void RendererImpl::draw(const std::vector<Emitter> &emitters) {
    clear_screen();
    draw_emitters(emitters);
    SDL_RenderPresent(m_renderer);
}


void RendererImpl::clear_screen() {
    SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 255);
    SDL_RenderClear(m_renderer);
}

void RendererImpl::draw_emitters(const std::vector<Emitter> &emitters) {
    for (auto &emitter : emitters) {
        draw_emitter(emitter);
    }
}

void RendererImpl::draw_emitter(const Emitter &emitter) {

    const auto &particles = emitter.get_particles();

    for (const auto &particle : particles) {
        draw_emitter_particle(particle);
    }
}

void RendererImpl::draw_emitter_particle(const Particle &particle) {
    SDL_Rect location;
    location.x = particle.get_x();
    location.y = particle.get_y();
    location.h = 1;
    location.w = 1;

    SDL_RenderCopy(m_renderer, particle.get_texture(), nullptr, &location);
}
