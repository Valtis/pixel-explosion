#pragma once

#include <vector>
#include <SDL.h>

class Emitter;
class Particle;

#define Renderer RendererImpl::instance()

class RendererImpl final {
public:

    ~RendererImpl();

    RendererImpl(const RendererImpl &) = delete;
    RendererImpl &operator=(const RendererImpl &) = delete;

    static RendererImpl &instance();

    SDL_Renderer *get_rendering_context() { return m_renderer; }

    void create_window(const char* title, Sint32 width, Sint32 height);

    void draw(const std::vector<Emitter> &emitters);

private:

    RendererImpl();

    void clear_screen();

    void draw_emitters(const std::vector<Emitter> &emitters);
    void draw_emitter(const Emitter &emitter);

    void draw_emitter_particle(const Particle &particle);

    SDL_Window *m_window;
    SDL_Renderer *m_renderer;
};
