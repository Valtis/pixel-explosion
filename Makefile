CXX = g++
SECURITY_FLAGS = -fstack-protector-all -fPIE -D_FORTIFY_SOURCE=2
SDL_CFLAGS = $(shell sdl2-config --cflags)
SDL_LDFLAGS = $(shell sdl2-config --libs)
MORE_FLAGS = -Wduplicated-cond -Wduplicated-branches -Wlogical-op -Wnull-dereference -Wshadow -Wundef -Wcast-align -Wsuggest-override -Wformat=2 -Wmisleading-indentation
CFLAGS = $(SDL_CFLAGS)
CXXFLAGS = -std=c++17 -Wall -Wextra -Werror $(SECURITY_FLAGS) $(MORE_FLAGS)
CPPFLAGS =
RELEASE_FLAGS = -O3 -flto
DEBUG_FLAGS = -g -Og
LDFLAGS = $(SDL_LDFLAGS)
IDIR = -Isrc


SRC = $(shell find . -name '*.cpp')
OBJ = $(subst .cpp,.o,$(SRC))
HEADER_DEPS = $(shell find . -name '*.d')
BIN = demo

.PHONY: clean all debug release verbosedebug

all: debug

release: CXXFLAGS += $(RELEASE_FLAGS)
release: $(BIN)

debug: CXXFLAGS += $(DEBUG_FLAGS)
debug: $(BIN)

verbosedebug: CPPFLAGS += -DDEBUG
verbosedebug: debug


$(BIN): $(OBJ)
	$(CXX) -o $(BIN) $(OBJ) $(CFLAGS) $(CXXFLAGS) $(LDFLAGS)

%.o: %.cpp
	$(CXX) $(IDIR) $(CPPFLAGS) $(CFLAGS) $(CXXFLAGS) -MMD -c $< -o $@


-include $(HEADER_DEPS)

clean:
	find . -type f -name "*.o" -delete
	find . -type f -name "*.d" -delete
	rm -f $(BIN)
